package projekakhirswing;

import config.Settings;
import menu.*;


/*
    Ini adalah program main dari project "Projek Akhir Swing"
*/
public class ProjekAkhirSwing {

    private static int kesempatan_login = 3;
    
    public static int getKesempatan_login() {
        return kesempatan_login;
    }
    
    public static void setKesempatan_login(int kesempatan_login) {
        ProjekAkhirSwing.kesempatan_login = kesempatan_login;
    }
    
    public static void main(String[] args) {
        Settings objSettings = new Settings();
        
        //Read file and add to arraylist
        new data.SoalFileManagement().readFile("soal.txt", true);
        new data.PendaftarFileManagement().readFile("pendaftar.txt", true);
        
        //soal.TestCpns2.setBukaTes(true);
        
        setLookAndFeel("Nimbus");
        MenuAwal menu = new MenuAwal();
        menu.setVisible(true);
        menu.setLocationRelativeTo(null);
        
    }
    
    private static void setLookAndFeel(String theme){
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if (theme.equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuAwal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuAwal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuAwal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuAwal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    
}
