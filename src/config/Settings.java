package config;

public class Settings {
    private static int toggleMode = 0; //UNSELECTED
    
    public static boolean isDarkModeSelected(){
        return Settings.toggleMode % 2 == 1;
    }

    public static void changeToggleMode() {
        Settings.toggleMode ++;
        if( !isDarkModeSelected() ){
            Settings.toggleMode = 0;
        }
    }
    
}
